import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgFormNombresComponent } from './ng-form-nombres.component';

describe('NgFormNombresComponent', () => {
  let component: NgFormNombresComponent;
  let fixture: ComponentFixture<NgFormNombresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgFormNombresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgFormNombresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
