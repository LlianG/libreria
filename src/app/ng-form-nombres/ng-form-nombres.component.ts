import { Component, OnInit } from '@angular/core';
import { PruebaClass } from './prueba.class';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-ng-form-nombres',
  templateUrl: './ng-form-nombres.component.html',
  styleUrls: ['./ng-form-nombres.component.css']
})
export class NgFormNombresComponent implements OnInit {
  formData: PruebaClass;

  constructor() {
    this.formData = new PruebaClass()
  }

  ngOnInit() {
  }

}
