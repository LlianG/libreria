import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgFormNombresComponent } from './ng-form-nombres/ng-form-nombres.component';


const routes: Routes = [
  {
    path: "",
    component: NgFormNombresComponent,
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
